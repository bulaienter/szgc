import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite';
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers';

import postcssPluginPx2rem from "postcss-plugin-px2rem";

//配置参数 
const px2remOptions = {
  rootValue: 16,  //换算基数， 默认100 ,也就是1440px ，这样的话把根标签的字体规定为1rem为50px,这样就可以从设计稿上量出多少个px直接在代码中写多少px了
  unitPrecision: 5, //允许REM单位增长到的十进制数字，其实就是精度控制
  propList   : ['*'],
  // propWhiteList: [], // 默认值是一个空数组，这意味着禁用白名单并启用所有属性。
  // propBlackList: [], // 黑名单
  // exclude:false,  //默认false，可以（reg）利用正则表达式排除某些文件夹的方法，例如/(node_module)/ 。如果想把前端UI框架内的px也转换成rem，请把此属性设为默认值
  // selectorBlackList: [], //要忽略并保留为px的选择器
  // ignoreIdentifier: false, //（boolean/string）忽略单个属性的方法，启用ignoreidentifier后，replace将自动设置为true。
  // replace: true, // （布尔值）替换包含REM的规则，而不是添加回退。
  mediaQuery: false, //（布尔值）允许在媒体查询中转换px
  minPixelValue: 0  //设置要替换的最小像素值(3px会被转rem)。 默认 0
}
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [Components({
    resolvers: [
      AntDesignVueResolver({
        importStyle: false, // css in js
      }),
    ],
  }),
    vue()
  ],
  server: {
    open:true,//是否自动打开浏览器，可选项
    cors:true,//允许跨域。
    // 设置代理
    proxy: {
      // 将请求代理到另一个服务器
      '/modelApi': {
        target: 'http://mi-rains.net/',//这是你要跨域请求的地址前缀
        changeOrigin: true,//开启跨域
        rewrite: path => path.replace(/^\/modelApi/, ''),//去除前缀api
      },
      '/iot': {
        target: 'http://10.18.1.47:8085/iot',//这是你要跨域请求的地址前缀
        changeOrigin: true,//开启跨域
        rewrite: path => path.replace(/^\/iot/, ''),//去除前缀api
      },
      '/mes': {
        target: 'http://10.18.1.47:8080/mes',//这是你要跨域请求的地址前缀
        changeOrigin: true,//开启跨域
        rewrite: path => path.replace(/^\/mes/, ''),//去除前缀api
      },
      '/api': {
        target: 'http://10.18.1.47:8081/api',//这是你要跨域请求的地址前缀
        changeOrigin: true,//开启跨域
        rewrite: path => path.replace(/^\/api/, ''),//去除前缀api
      },
      '/getjson': {
        target: 'http://47.113.200.153:31301',//这是你要跨域请求的地址前缀
        changeOrigin: true,//开启跨域
        rewrite: path => path.replace(/^\/getjson/, ''),//去除前缀api
      }
    }
  },
  css: {
    postcss: {
      plugins: [postcssPluginPx2rem(px2remOptions)]
    }
  }

})
