import * as THREE from "three";
import gsap from "gsap";
import {selectStorageOfCargo} from "../util/api";

// 默认地址
 // this.goodsArray=[{x:1,y:10,z:1},{x:1,y:8,z:9},{x:2,y:9,z:11},{x:2,y:10,z:15},
        // {x:2,y:1,z:5},{x:3,y:2,z:5},{x:3,y:3,z:5},{x:3,y:4,z:5},{x:3,y:4,z:5},{x:3,y:4,z:5}]
export default class Storage {
    constructor(goodsArray,goodsAll,goods1,goods001) {
        this.goodsArray=goodsArray
        this.goodsAll=goodsAll;
        this.goods1=goods1;
        this.goods001=goods001;
        selectStorageOfCargo().then(res=>{
            this.optionsArr=res.data;
            this.getEl(0)
            // 下架
            // this.goodsArray.forEach((item,i)=>{
            //     let oz2=7.3+(this.optionsArr[i].z*1.7)+(Math.floor(this.optionsArr[i].z/4)*0.2)
            //     let oy2=this.optionsArr[i].y==1?0.4:this.optionsArr[i].y>=7?0.2+((this.optionsArr[i].y-1)*0.5):0.3+((this.optionsArr[i].y-1)*0.5)
            //     item.position.set(-95,oy2,oz2)
            // })
        })
       


       


      
    } 
     getGsap (mesh, name, obj, onComplete,time)  {
        if(!mesh)return;
        gsap.to(mesh[name], {
            ease:'none',
            ...obj,
            duration: time?time:2,
            onComplete
        })
    }
    getEl(index) {


        if (index >= this.goodsArray.length) {
            return
        }

        // 下架
        // let oz3=8.3+(this.optionsArr[index].z*1.7)+(Math.floor(this.optionsArr[index].z/4)*0.2)
        // this.bindOffGoods(index, {z: oz3, y: -2.1+(this.optionsArr[index].y*0.4)}, 'start', () => {
        //     this.bindOffGoods(index, {z: 45.5, y: -1.7}, 'progress', () => {
        //     index++;
        //         this.getEl(index)
        //   })
        // })

        
       

        // 上架
        let oz=8.3+(this.optionsArr[index].z*1.7)+(Math.floor(this.optionsArr[index].z/4)*0.2)
        this.bindGoods(index, {z: 45.5, y: -1.7}, 'start', () => {
            this.bindGoods(index, {z: oz, y: -2.3+(this.optionsArr[index].y*0.4)}, 'progress', () => {
            index++;
            this.getEl(index)
          })
        })
    }
    bindOffGoods(index, obg, type, onComplete){
        let item=this.goodsArray[index];
        this.getGsap(this.goodsAll, 'position', {z: obg.z}, () => {
            this.getGsap(this.goods1, 'position', {y: obg.y}, () => {
                this.getGsap(this.goods001, 'position', {x: -1}, () => {
                    this.getGsap(this.goods001, 'position', {x: 0.1}, () => {
                        this.getGsap(this.goods1, 'position', {y: obg.y}, onComplete)
                        this.getGsap(this.goods001, 'position', {y: obg.y}, () => {
                        })
                    })
                    if (type == 'start') {
                        this.getGsap(item, 'position', {x: -94}, null)
                    }
                })
                if (type == 'progress') {
                    this.getGsap(item, 'position', {x: -95}, () => {})
                }
            })
            this.getGsap(this.goods001, 'position', {y: obg.y}, () => {})
            if (type == 'progress') {
                this.getGsap(item, 'position', {y:1}, null)
            }
        })
        if (type == 'progress') {
            this.getGsap(item, 'position', {z: obg.z - 1}, () => {

        })
        }
    }
    bindGoods  (index, obg, type, onComplete)  {
//   货架
        // this.getGsap(goodsAll.value,'position',{z:45},onComplete)

        let item=this.goodsArray[index];
        this.getGsap(this.goodsAll, 'position', {z: obg.z}, () => {
            this.getGsap(this.goods1, 'position', {y: obg.y}, () => {
                this.getGsap(this.goods001, 'position', {x: -1}, () => {
                    this.getGsap(this.goods001, 'position', {x: 0.1}, () => {

                        this.getGsap(this.goods1, 'position', {y: obg.y}, onComplete)
                        this.getGsap(this.goods001, 'position', {y: obg.y}, () => {

                        })
                        // if (type == 'progress') {
                        //   console.log(888,0.4+(arr[index].y*0.5),-2.1+(arr[index].y*0.4))
                        //   // 0.4+(arr[index].y*0.5)
                        //   this.getGsap(item, 'position', {y:0.2+(arr[index].y*0.4) }, () => {
                        //
                        //   })
                        // }

                    })
                    if (type == 'start') {
                        this.getGsap(item, 'position', {x: -94}, () => {

                        })
                    }

                })
                if (type == 'progress') {
                    this.getGsap(item, 'position', {x: -95}, () => {})
                }
            })
            this.getGsap(this.goods001, 'position', {y: obg.y}, () => {

            })
            if (type == 'progress') {
                // 0.2+(arr[index].y*0.4) (0.2+((arr[index].y-1)*0.5)
                let oy=this.optionsArr[index].y==1?0.4:this.optionsArr[index].y>=7?0.2+((this.optionsArr[index].y-1)*0.5):0.3+((this.optionsArr[index].y-1)*0.5)
                this.getGsap(item, 'position', {y:oy}, () => {

                })
            }
        })
        if (type == 'progress') {
            this.getGsap(item, 'position', {z: obg.z - 1}, () => {

            })
        }


    }
}