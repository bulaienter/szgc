// 来自第三方
import {
    EffectComposer,
    RenderPass,
    SelectiveBloomEffect,
    BlendFunction,
    EffectPass,
    SMAAEffect,
    SSAOEffect,
    NormalPass,
    GodRaysEffect,
    OutlineEffect,
} from "postprocessing";
import {SSREffect} from "screen-space-reflections";
export default class Comper {
    constructor(scene,renderer,camera,composer,outlineEffect,bloomEffect) {
        this.scene=scene;
        this.renderer=renderer;
        this.camera=camera;
        this.composer=composer
        this.outlineEffect=outlineEffect
        this.bloomEffect = bloomEffect;
        this.getComper()
    }
    getComper(){
        // 实例化后期处理


        this.composer = new EffectComposer(this.renderer);
        this.composer.addPass(new RenderPass(this.scene, this.camera));

        // 发光
        this.bloomEffect = new SelectiveBloomEffect(this.scene, this.camera, {
            blendFunction: BlendFunction.ADD,
            mipmapBlur: true,
            luminanceThreshold: 0.7,
            luminanceSmoothing: 0.3,
            intensity: 10.0
        })
        // 提升抗锯齿效果
        const smaaEffect = new SMAAEffect()

        // 添加环境光遮蔽
        const normalPass = new NormalPass(this.scene, this.camera);
        const ssaoEffect = new SSAOEffect(this.camera, normalPass.texture, {
            blendFunction: BlendFunction.MULTIPLY,
            samples: 30,
            rings: 3,
            luminanceInfluence: 0.1,
            radius: 0.005,
            bias: 0,
            intensity: 0

        })

        // const shaderEffect=new ShaderPass( GammaCorrectionShader )

        const options = {
            intensity: 1,
            exponent: 1,
            distance: 2,
            fade: 7,
            roughnessFade: 10,
            thickness: 0,
            ior: 1.45,
            maxRoughness: 0,
            maxDepthDifference: 0,
            blend: 0.9,
            correction: 1,
            correctionRadius: 1,
            blur: 0,
            blurKernel: 0,
            blurSharpness: 0,
            jitter: 0,
            jitterRoughness: 0,
            steps: 20,
            refineSteps: 5,
            missedRays: true,
            useNormalMap: true,
            useRoughnessMap: true,
            resolutionScale: 1,
            velocityResolutionScale: 1
        }

        // 添加屏幕空间反射
        const ssrEffect = new SSREffect(this.scene, this.camera, options);

        // const gui=new SSRDebugGUI(ssrEffect,options)
        // scene.add(gui)


        // 发光
        this.outlineEffect = new OutlineEffect(this.scene, this.camera, {
            blendFunction: BlendFunction.ADD,
            edgeStrength: 5,
            pulseSpeed: 0,
            visibleEdgeColor: 0xffffff,
            hiddenEdgeColor: 0x22090a,
            blur: false,
            xRay: true,
            usePatternTexture: false
        })

        // 创建效果通道
        const effectPass = new EffectPass(this.camera, this.bloomEffect,  smaaEffect, ssaoEffect,  this.outlineEffect, ssrEffect)
        this.composer.addPass(effectPass);
    }
}