import * as THREE from "three";

export default class PointClick {
    constructor(screenDom,camera,scene) {
        this.screenDom=screenDom
        this.camera=camera
        this.scene=scene
    }
 onMouseClick = (event) => {
    this.raycaster = new THREE.Raycaster();
    let mouse = new THREE.Vector2();
    // let div3D = document.getElementById('container');
    // 通过鼠标点击的位置计算出raycaster所需要的点的位置，以屏幕中心为原点，值的范围为-1到1.
    let div3DLeft = this.screenDom.getBoundingClientRect().left;
    let div3DTop = this.screenDom.getBoundingClientRect().top;
    mouse.x = (event.clientX - div3DLeft) / this.screenDom.clientWidth * 2 - 1;
    mouse.y = -((event.clientY - div3DTop) / this.screenDom.clientHeight) * 2 + 1;
    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
     this.raycaster.setFromCamera(mouse, this.camera);
    // 获取raycaster直线和所有模型相交的数组集合
    this.raycastMeshes(this.clickApp, this.raycaster);
}
 raycastMeshes  (callback, raycaster)  {
    let intersects = [];
    // 获取整个场景
    let theScene = this.scene || new THREE.Scene();
    // 获取鼠标点击点的射线
    let theRaycaster = raycaster || new THREE.Raycaster();
    // 对场景及其子节点遍历

    for (let i in theScene.children) {
        // 如果场景的子节点是Group或者Scene对象
        if (
            theScene.children[i] instanceof THREE.Group ||
            theScene.children[i] instanceof THREE.Scene
        ) {
           
            // 场景子节点及其后代，被射线穿过的模型的数组集合
            // intersects = theRaycaster.intersectObjects(theScene.children[i].children, true)
            let rayArr = theRaycaster.intersectObjects(
                theScene.children[i].children,
                true
            );
            intersects.push(...rayArr);
        } else if (theScene.children[i] instanceof THREE.Mesh) {
            
            // 如果场景的子节点是Mesh网格对象，场景子节点被射线穿过的模型的数组集合
            let caster=theRaycaster.intersectObject(theScene.children[i])
            
            if(caster.length>0){
                intersects.unshift(caster[0])
            }
            
        }
    }
    intersects = this.filtersVisibleFalse(intersects); // 过滤掉不可见的
    
    // 被射线穿过的模型的数组集合
    if (intersects && intersects.length > 0) {
        return callback(intersects);
    }
    return null;
}
// 有些对象是包含了很多children的所以需要遍历，并push到intersects数组去重。
 filtersVisibleFalse  (arr) {
    let arrList = arr;
    if (arr && arr.length > 0) {
        arrList = [];
        arr.forEach((item, i) => {
            if (!!item&&item.object&&item.object.visible) {
                
                arrList.push(item);
            }
        })
    }
    return arrList;
}
// 点击到位置的
 clickApp  (intersects)  {
   
    if (intersects[0].object !== undefined) {
        const mesh = intersects[0].object;
        // outlineEffect.selection.set([mesh])
        // bloomEffect.selection.set([mesh])
        // console.log( mesh.name, '这就是成功点击到的对象了~');
        if (mesh.name.includes('监控器') || mesh.name.includes('顶') || mesh.name.includes('立方体') || mesh.name.includes('外壳')|| mesh.name.includes('平面002_1')
        || mesh.name.includes('网格015')|| mesh.name.includes('工作区特殊挂片')|| mesh.name.includes('custom圆')) {
            mesh.userData.onClick();
        }
    }
}

}