import * as THREE from "three";
import {OrbitControls} from "three/addons/controls/OrbitControls.js";
import {RGBELoader} from "three/addons/loaders/RGBELoader.js";

export default class Start {
    constructor(clock, camera, scene, renderer, controls, texture, rbgeLoader,width,height,lightHeight) {
        this.clock = clock
        this.camera = camera
        this.scene = scene
        this.renderer = renderer
        this.controls = controls
        this.texture = texture
        this.rbgeLoader = rbgeLoader
        this.width=width?width:window.innerWidth
        this.height=height?height:window.innerHeight
        this.lightHeight=lightHeight?lightHeight:2000
        this.init()
    }

    init() {

        this.clock = new THREE.Clock();
        // 创建场景
        this.scene = new THREE.Scene();

        //创建相机 透视相机
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, this.lightHeight);
        this.camera.position.set(200, 400, 600);
        this.camera.updateProjectionMatrix()
        this.camera.lookAt(this.scene.position);
        this.scene.add(this.camera);

        // 初始化渲染器
        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
            logarithmicDepthBuffer: true,
            powerPreferencec: 'hight-performance',
            outputEncoding: "sRGBEncoding"
        });
        // 设置渲染 的尺寸大小
        this.renderer.setSize(this.width, this.height);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.outputColorSpace = THREE.sRGBEncoding;
        this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFShadowMap;


        // 轨道控制器
        this.controls = new OrbitControls(this.camera, this.renderer.domElement)
        // 设置控制器阻尼，让控制器更有真实效果,必须在动画循环里面调用 update
        this.controls.enableDamping = true;
        // this.controls.minPolarAngle = 0.3
        // this.controls.maxPolarAngle = 1.5

        // 添加环境纹理
        this.rbgeLoader = new RGBELoader();
        this.rbgeLoader.load("./model/texture/noon_grass_1k.hdr", (texture) => {
            if(!this.texture)return;
            this.texture.mapping = THREE.EquirectangularReflectionMapping;
            this.scene.background = this.texture;
            this.scene.environment = this.texture;
            this.scene.backgroundBlurriness = 1;//模糊
        });


        // const texLoader = new THREE.TextureLoader();
        // let material =null;
        // const textureFloor = texLoader.load(
        //     new URL(`../../assets/floor.jpeg`, import.meta.url).href,
        // );
        // textureFloor.wrapS = THREE.RepeatWrapping;
        // textureFloor.wrapT = THREE.RepeatWrapping;
        // textureFloor.repeat.set(100, 100);
        // material = new THREE.MeshBasicMaterial({
        //   color: 0x777700,
        //   map: textureFloor
        // });
        //
        // const geometry = new THREE.PlaneGeometry(2000,2000,);
        // const mesh = new THREE.Mesh(geometry, material);
        // mesh.position.set(0,-0.1,0)
        // mesh.rotateX(-Math.PI / 2);
        // scene.add(mesh);

        const grid = new THREE.GridHelper(500, 50, 0xffffff, 0xffffff);
        grid.material.opacity = 0.2;
        grid.material.transparent = true;
        this.scene.add(grid);

        this.lightRendering()

    }

    lightRendering() {
        // 灯光-环境光
        this.scene.add(new THREE.AmbientLight(0xffffff, 3))
        // 聚光灯
        let spotLight = new THREE.SpotLight(0xffffff);
        spotLight.position.set(100, 400, 0);
        spotLight.shadow.camera.near = 0.5;
        spotLight.shadow.camera.far = 300;
        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 2048;
        spotLight.shadow.mapSize.height = 2048;
        spotLight.shadow.mapSize.bias = -0.00001;
        // spotLight.angle = Math.PI / 2;
        spotLight.intensity = 5;
        this.scene.add(spotLight)





    }

}