import * as THREE from "three";
import gsap from "gsap";

let plcStatus1 = 0;
let plcStatus2 = 0;
let isMachinery = false;
let isOnly = false;
let isOnly2 = false;
let isAdd = false;
let isAdd2 = false;
let isMove = false;
export default class GetJson {
  constructor(data_result, data) {
    this.machinery1 = data.machinery1;
    this.machinery2 = data.machinery2;
    this.mixer = data.mixer;
    this.modelAnimation = data.modelAnimation;
    this.coupon2Position = data.coupon2Position;
    this.coupon3Position = data.coupon3Position;
    this.support = data.support;
    this.special = data.special;
    this.forklift = data.forklift;
    
    this.gethcInit(data_result);
  }
  //初始化
  gethcInit(data_result) {
    let data = data_result.data;
    let move1x = data.contentOne.hc01.split(",")[0];
    let move2x = data.contentTwo.hc02.split(",")[0];
   
    if (!isAdd && move1x > 2 && data.contentOne.location!='0'&&!isOnly) {
      console.log("woshanglai",data.contentOne.location)
      this.machinery2[0].position.y = 5.8;
      // this.machinery2[0].position.x = -move1x * 13 - 3;
      // this.machinery2[1].position.x = -move1x * 13;
    }
    if (!isAdd2 && !!data.contentTwo.location) {
      this.machinery1[0].position.y = 6.0;
      // this.machinery1[0].position.x = -move2x * 13 + 3;
      // this.machinery1[1].position.x = -move2x * 13+6;
    }
    this.getJson(this.machinery2, data, false, 3);
    this.getJson(this.machinery1, data, true, 3);
  }
  //动画
  getGsap(mesh, name, obj, onComplete, time) {
    if (!mesh) return;
    gsap.to(mesh[name], {
      ease: "none",
      ...obj,
      duration: time ? time : 2,
      onComplete,
    });
  }
  //   喷淋槽动画
  plcAnimation(res,handstand) {
    let plcnum1 = 0;
    let plcnum2 = 0;
    let playArr = [];
    let playArr2 = [];
    let action = null;
    let action2 = null;

    for (var i = 1; i <= 8; i++) {
      if (res.contentOne["plc0" + i] == 1) {
        plcnum1 = i;
      } else if (res.contentTwo["plc0" + i] == 1) {
        plcnum2 = i;
      }
    }
   
    if (plcnum1 != 0&&!handstand) {
      plcStatus1 = plcnum1;
     
      playArr = this.modelAnimation.filter(
        (item) => item.name == "浸泡装置" + (8 + (9 - plcnum1)) + "号开"
      );
      if (!playArr.length) return;
      action = this.mixer.clipAction(playArr[0]);
      action.clampWhenFinished = true;
      action.loop = THREE.LoopOnce;
      action.play();
    } else if (plcnum1 == 0 && plcStatus1 != 0&&!handstand) {
      playArr = this.modelAnimation.filter(
        (item) => item.name == "浸泡装置" + (8 + (9 - plcStatus1)) + "号关"
      );
      if (!playArr.length) return;
      action = this.mixer.clipAction(playArr[0]);
      action.clampWhenFinished = true;
      action.loop = THREE.LoopOnce;
      action.play();
      this.mixer.addEventListener("finished", () => {
        plcStatus1 = 0;
      });
    }

    if (plcnum2 != 0&&handstand&&plcnum2!=plcStatus2) {
      
      plcStatus2 = plcnum2;
      
      playArr2 = this.modelAnimation.filter(
        (item) => item.name == "浸泡装置" + (9 - plcnum2) + "号开"
      );
      if (!playArr2.length) return;
      action2 = this.mixer.clipAction(playArr2[0]);
      action2.clampWhenFinished = true;
      action2.loop = THREE.LoopOnce;
      action2.play();
    } else if (plcnum2 == 0 && plcStatus2&&handstand) {
      playArr2 = this.modelAnimation.filter(
        (item) => item.name == "浸泡装置" + (9 - plcStatus2) + "号关"
      );
      if (!playArr2.length) return;
      action2 = this.mixer.clipAction(playArr2[0]);
      action2.clampWhenFinished = true;
      action2.loop = THREE.LoopOnce;
      action2.play();   
        this.mixer.addEventListener("finished", () => {
          plcStatus2 = 0;
        });
    }
  }

  getJson(machineryplay, res, handstand, time) {
    // apiJson().then(res=>{
    let data = !handstand ? res.contentOne.hc01 : res.contentTwo.hc02;
    let arr = data.split(",");

    // if(handstand){machineryplay[0].visible=false;}
    let moveX = !handstand
      ? -(Number(arr[0]) * 13).toFixed(1)
      : -(Number(arr[0]) * 13 + 6).toFixed(1);
    let isDown1 = res.contentOne.isDown;
    let isDown2 = res.contentTwo.isDown;

    let isFollow1 = res.contentOne.location;
    let isFollow2 = res.contentTwo.location;

    this.plcAnimation(res,handstand)
    // if (!handstand) {
    //   console.log(456, moveX, arr[0]);
    //   console.log(888, isDown1, isFollow1, isAdd );
    // }
    // if (handstand) {
    //   console.log(456, moveX, arr[0]);
    //   console.log(888, isDown2, isFollow2, isAdd2 );
    // }
    this.getGsap(
      machineryplay[1],
      "position",
      { x: moveX },
      () => {
        
        if (moveX == -75.4 && !handstand && !isDown1) {
          isMachinery = true;
        }

        if (moveX == -78 && !handstand && isDown1) {
          // 第一个流水线结束动画之后
          if (isOnly) {
            return;
          }
          isOnly = true;

          this.getGsap(
            this.support,
            "position",
            { z: 35 },
            () => {
              machineryplay[0].position.x = this.support.position.x - 0.5;
              
            },
            2
          );
          this.getGsap(
            machineryplay[0],
            "position",
            { y: 2.4, delay: 3 },
            () => {
              this.getGsap(
                machineryplay[0],
                "position",
                { z: 17.5 },
                () => {
                  this.getGsap(
                    machineryplay[0],
                    "position",
                    { delay: 5 },
                    () => {
                      machineryplay[0].position.set(
                        this.coupon2Position.x,
                        this.coupon2Position.y,
                        this.coupon2Position.z
                      );
                      if(this.special)this.special.visible = true;    
                      isOnly = false;
                    },
                    2
                  );
                },
                2
              );
              this.getGsap(this.support, "position", { z: 12 }, null, 2);
            },
            2
          );
        } else if (-12.5 == moveX && handstand) {
          // 第二条流水线完结
          if (isOnly2) return;
          isOnly2 = true;
          this.getGsap(
            machineryplay[0],
            "position",
            { x: -8.5, y: 2.2, delay: 5 },
            () => {
              // 进度在这里，尝试隐藏，矫正结束后的穿回来bug
              if(this.coupon3Position)this.coupon3Position.visible = true;
              
              let action = this.mixer.clipAction(this.forklift);
              action.play();
              action.loop = THREE.LoopOnce;
              machineryplay[0].position.set(
                this.support.position.x,
                this.support.position.y,
                machineryplay[1].position.z
              );
              machineryplay[0].visible = false;
              isOnly2 = false;  
            },
            2
          );
        } else if (
          (isDown1==1 && !handstand) ||
          (handstand && isDown2==1)
        ) {
          // 向下

          if ((isAdd && !handstand) || (isAdd2 && handstand)) {
            return;
          }
         
          if (handstand) {
            isAdd2 = true;
          } else {
            isAdd = true;
          }
          console.log("向下啊", isFollow1,isAdd);
          // machineryplay[0].visible=true
          if(!handstand){
            this.getGsap(machineryplay[2], "position", { y: -0.1 }, null, 1);
            this.getGsap(
              machineryplay[3],
              "position",
              { y: -1.3 },
             null,
              1
            );
          }else{
            // this.getGsap(machineryplay[2], "position", { y: -0.1 }, null, 1);
            this.getGsap(
              machineryplay[2],
              "position",
              { y: -1.3 },
             null,
              1
            );
          }
          // this.getGsap(machineryplay[2], "position", { y: -0.1 }, null, 1);
          // this.getGsap(
          //   machineryplay[3],
          //   "position",
          //   { y: -1.3 },
          //   () => {
          //     // isMove = true;
          //     // if (!handstand) {
          //     //   isOnly = false;
          //     // } else {
          //     //   isOnly2 = false;
          //     // }
          //   },
          //   1
          // );
          if ((isFollow1 != 0&&!handstand)||(isFollow2 != 0&&handstand)) {
            console.log("挂件下降");
            this.getGsap(machineryplay[0], "position", { y: 4.5 }, null, 1);
          }
        } else if (
          (isDown1==0 && isAdd && !handstand) ||
          (handstand && isDown2==0  && isAdd2)
        ) {
          console.log("收起来");
         
          if(!handstand){
            this.getGsap(machineryplay[2], "position", { y: 1.1 }, null, 1);
            this.getGsap(machineryplay[3], "position", { y: -0.14 }, null, 1);
          }else{
            machineryplay[0].visible=true;
            this.getGsap(machineryplay[2], "position", { y: -0.14 }, null, 1);
          }
          
          this.getGsap(
            machineryplay[0],
            "position",
            { y: 5.8 },
            () => {
              if (handstand) {
                isAdd2 = false;
              } else {
                isAdd = false;
              }
            },
            1
          );
        } else {
        }
      },
      time
    );
    if (
      (!handstand && isFollow1 != 0 && !isOnly) ||
      (handstand && isFollow2 != 0 && !isOnly2)
    ) {
      this.getGsap(machineryplay[0], "position", { x: moveX - 3 }, null, time);
    }

    // })
  }


  
  // 前端模拟
  timeNew = () => {
    if (isIndex == 13) {
      isIndex = 0;
    }
    if (isIndex == 0 || isIndex == 12) {
      isAdd.value = false;
    }

    if (isMachinery.value) {
      if (isIndex2 == 13) {
        isIndex2 = 0;
      }
      if (isIndex2 == 0 || isIndex2 == 12) {
        isAdd2.value = false;
      }
    }

    let jsonNum = [1.5, 1.5, 2.0, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6, 6];
    let jsonNum2 = [5.5, 5.5, 5.0, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5, 0.5];
    let dataNew = { messagecontent: { hc01: `${jsonNum[isIndex]},0,0` } };
    let dataNew2 = { messagecontent: { hc01: `${jsonNum2[isIndex2]},0,0` } };
    // getJson(this.machinery1,dataNew2,true,5)

    getJson(this.machinery2, dataNew, false, 5);
    isMachinery.value ? getJson(this.machinery1, dataNew2, true, 5) : "";
    isIndex++;
    isMachinery.value ? isIndex2++ : "";
  };
  // setInterval(()=>{
  //
  //   timeNew()
  //
  // },5000)
}
