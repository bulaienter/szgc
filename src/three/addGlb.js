import {GLTFLoader} from "three/addons/loaders/GLTFLoader.js";
import {DRACOLoader} from "three/addons/loaders/DRACOLoader.js";
import * as THREE from "three";
import {CSS3DObject, CSS3DRenderer} from "three/addons/renderers/CSS3DRenderer.js";

export default class AddGlb {
    constructor(data, isFlag) {
        if(isFlag){return}
        this.scene = data.scene;
        this.camera = data.camera;
        this.controls=data.controls;
        this.rbgeLoader = data.rbgeLoader;
        this.boxList = data.boxList;
        this.css3dObjectEl = data.css3dObjectEl;
        this.css3drender = data.css3drender;
        this.css3dObject = data.css3dObject;
        this.special = data.special;
        this.mixer = data.mixer;
        this.forklift = data.forklift;
        this.coupon2Position = data.coupon2Position;
        this.coupon3Position= data.coupon3Position;
        this.closure = data.closure;
        this.support = data.support;
        this.machinery1 = data.machinery1;
        this.machinery2 = data.machinery2;
        this.machinery = data.machinery;
        this.goodsArray = data.goodsArray;
        this.goods1 = data.goods1
        this.goods001 = data.goods001
        this.goodsAll = data.goodsAll;
        this.modelAnimation=data.modelAnimation
        this.lightArr=data.lightArr

        // if (isFlag) {
        //     this.loaderGLB()
        // }

       

    }

    createTag(object3d, isVideo) {
        
        // 创建各个区域的元素
        let mapPicSrc = new URL(`../assets/close.png`, import.meta.url).href;
        let videogc = new URL(`../assets/videogc.mp4`, import.meta.url).href;
        const element = document.createElement("div");
        element.className = "elementTag";
        if (isVideo) {
            element.innerHTML = `
      <div class="elementContent">
      <div class="flex-between"><h3>${object3d.name}</h3>
      <img class="flex-img" src="${mapPicSrc}" alt="" onclick="bindClose()">
      </div>
            <div class="video">
            <video src="${videogc}" controls autoplay ></video>
            </div>
      </div>
    `;
        } else {
            element.innerHTML = `
      <div class="elementContent">
      <div class="flex-between"><h3>${object3d.name}</h3>
      <img class="flex-img" src="${mapPicSrc}" alt="" onclick="bindClose()">
      </div>
      <div>生产状况：${object3d.status==1?'已开启':'未开启'}</div>
      <div>设备编号：${object3d.serialNumber}</div>
      </div>
    `;
        }

        //
        const objectCSS3D = new CSS3DObject(element);
        objectCSS3D.scale.set(0.1, 0.1, 0.1);       
        // objectCSS3D.position.copy(object3d.position);
      
        return objectCSS3D;
    }

    render3d() {
        this.css3drender = new CSS3DRenderer();
        this.css3drender.setSize(window.innerWidth*0.67, window.innerWidth*0.28);
        this.css3drender.domElement ? document.querySelector("#cssrender").appendChild(this.css3drender.domElement) : '';
    }

    getLoadGlb() {
        let gltfLoader = new GLTFLoader();
        const dracoLoader = new DRACOLoader()
        dracoLoader.setDecoderPath('/draco/')
        gltfLoader.setDRACOLoader(dracoLoader)
        gltfLoader.load("/modelApi/guajian3.glb", (gltf) => {
            const meshChild = gltf.scene;
            this.special.children.length = 0;

            let positionZ=JSON.parse(JSON.stringify(this.special.position.z))
            meshChild.traverse((child) => {
                if (child.name.includes('特殊挂件集合')) {
                    child.name==this.special.name
                    child.position.z=positionZ
                    console.log(5555,child.position,this.special.position)
                    this.special.copy(child)
                }
            })
            // this.scene.add(meshChild);
        })
    }
    // getLoadGlb() {
    //     let gltfLoader = new GLTFLoader();
    //     const dracoLoader = new DRACOLoader()
    //     dracoLoader.setDecoderPath('/draco/')
    //     gltfLoader.setDRACOLoader(dracoLoader)
    //     gltfLoader.load("/modelApi/guajian3.glb", (gltf) => {
    //         const meshChild = gltf.scene;
    //         this.special.children.length = 0;

    //         let positionZ=JSON.parse(JSON.stringify(this.special.position.z))
    //         meshChild.traverse((child) => {
    //             if (child.name.includes('特殊挂件集合')) {
    //                 child.name==this.special.name
    //                 child.position.z=positionZ
    //                 console.log(5555,child.position,this.special.position)
    //                 this.special.copy(child)
    //             }
    //         })
    //         // this.scene.add(meshChild);
    //     })
    // }

    loaderGLB() {
       return new Promise((resolve, reject) => {
            this.camera.position.set(30, 80, 110);
        //    this.camera.position.set(-122, 13, 25);
            let gltfLoader = new GLTFLoader();
            const dracoLoader = new DRACOLoader()
            dracoLoader.setDecoderPath('/draco/')
            gltfLoader.setDRACOLoader(dracoLoader)
            gltfLoader.load("./model/gc1213.glb", (gltf) => {
                
                const mesh = gltf.scene;
                this.boxList.push(mesh)
                console.log(mesh,"chushihua")
                // machineryGroup.value=new THREE.Group()
                // 处理动画
                this.modelAnimation = gltf.animations || [];
            
                let decline=[]
                // console.log(this.modelAnimation,77778)
                // 如果当前模型有动画则默认播放第一条动画
                if (this.modelAnimation.length) {
                    let arr = []
                    this.mixer = new THREE.AnimationMixer(mesh);
                    for (let i = 0; i < this.modelAnimation.length; i++) {
                        // let tracks = modelAnimation[i].tracks
                        // if (tracks) {
                        //     for (let j = 0; j < tracks.length; j++) {
                        //         if (modelAnimation[i].name.includes('运货车集合2') || modelAnimation[i].name.includes('AVG小车') ||modelAnimation[i].name.includes('门')) {
                        //
                        //             this.forklift.tracks.push(modelAnimation[i])
                        //         }
                        // if (this.modelAnimation[i].name=='浸泡装置3号开') {
                        //     decline.push(this.modelAnimation[i])
                        //     const action = this.mixer.clipAction(this.modelAnimation[i]);
                        //     action.clampWhenFinished = true;
                        //     action.loop = THREE.LoopOnce;
                        //     // const action = this.mixer.clipAction(this.forklift);
                        //     action.play();
                        // }
                        // if (this.modelAnimation[i].name=='浸泡装置1号关') {
                        //     setTimeout(() => {
                        //         const action2 = this.mixer.clipAction(this.modelAnimation[i]);
                        //     action2.clampWhenFinished = true;
                        //     action2.loop = THREE.LoopOnce;
                        //     // const action = this.mixer.clipAction(this.forklift);
                        //     action2.play();
                        //     }, 5000);
                            
                        // }
                        const action = this.mixer.clipAction(this.modelAnimation[i]);
                        // const action = this.mixer.clipAction(this.forklift);
                        // action.play();
                    }
                    // const action1 = this.mixer.clipAction(decline);
                    // action1.play();
                }
                // '工作区机械台框架001',
                let machineryarr = ["工作区特殊挂件集合2",'工作区机械台合集','工作区机械台装置']
                let machineryarr2 = ["工作区特殊挂件集合2001",'工作区机械台2号合集', '工作区机械台框架2', '工作区机械台装置2']
         
                this.rbgeLoader.load("./model/texture/living.hdr", (texture) => {

                    texture.mapping = THREE.EquirectangularReflectionMapping;

                    mesh.traverse((child) => {
                        // child.receiveShadow = true;
                        child.castShadow = true;

                            // ||child.name.includes( "平面002")
                        if (child.name.includes('钢架') || child.name.includes('立方体框')) {
                            child.visible = false;
                        }

                        if (child.isMesh && child.name !== "平面001") {
                            child.castShadow = true;
                            // child.receiveShadow = true;
                            child.material.envMapIntensity = 0.2;
                            child.material.envMap = texture;
                        }
                        if (child.name === "平面001") {
                            child.material = new THREE.MeshLambertMaterial({
                                color: child.material.color,
                                polygonOffset: true,
                                polygonOffsetFactor: 1,  // 正值将面向前推
                                polygonOffsetUnits: 1,
                                castShadow: true,
                                roughness: 1
                            });

                            child.castShadow = true;
                            child.receiveShadow = true;
                            // child.material.roughness = 0.6;


                        }
                        if (child.name.includes('外壳') || child.name.includes('顶')) {
                            this.closure.push(child)
                        }
                        if(child.name.includes('灯')){
                            // child.visible=false
                            child.children.forEach(item=>{
                                this.lightArr.push(item)
                            })
                            
                        }
                        if (child.name.includes('Cone')) {
                            child.material.emissive = 'orange';
                            child.material.color = '0xffffff';
                            // child.material=new THREE.MeshLambertMaterial({
                            //   emissive:0xffffff,
                            //   emissiveIntensity:1
                            // })
                        }
                        if (child.name == '工作区特殊挂件集合2') {
                   
                            this.special = child;
                            // child.visible=false;
                        }
                        // if (child.name=='平面002_1') {
                        //     child.visible=false;
                        // }


                        if (child.name == '工作区特殊挂件集合002'||child.name == '工作区特殊挂件集合2001') {
                            this.coupon2Position = JSON.parse(JSON.stringify(child.position))
                        }
                        if (child.name=='喷漆线特殊') {
                            this.coupon3Position = child;
                            child.visible=false

                        }

                        if (child.name == '工作区下料装置前') {
                            this.support = child;
                        }

                        if (machineryarr.indexOf(child.name) != -1) {
                            child.sortIndex=machineryarr.indexOf(child.name)
                            this.machinery1.push(child)

                        }
                        if (machineryarr2.indexOf(child.name) != -1) {
                            child.sortIndex=machineryarr2.indexOf(child.name)
                            this.machinery2.push(child)

                        }


                        if (child.name == '网格006') {
                            this.machinery = child;
                            // this.css3dObjectEl = this.createTag(child, false);
                            // this.css3dObjectEl.visible = true;
                            // this.css3dObjectEl.position.set(-30, 0, 16)
                            // this.scene.add(this.css3dObjectEl);
                        }
                        if (child.name.includes('顶') || child.name.includes('外壳') || child.name.includes('立方体')||child.name.includes('平面002_1')) {
                            // object.add(css3dObject);
                            child.userData.onClick = () => {
                                child.visible = false;
                            }
                        }

                        if (child.name.includes('监控器004')) {

                            this.css3dObject = this.createTag(child, true);
                            this.css3dObject.visible = false;
                            // const Euler = new THREE.Euler(this.camera.position.x, this.camera.position.y, this.camera.position.z);

                            this.css3dObject.position.set(child.position.x,child.position.y ,child.position.z )
                            // this.css3dObject.y = child.position.y + 9;
                            // this.css3dObject.z = child.position.z + 8;
                            // Euler.order = 'Y';
                            // this.css3dObject.rotation.copy(Euler)

                            //
                            this.scene.add(this.css3dObject);
                        }
                        if (child.name.includes('监控器')) {
                            child.scale.set(1.5, 1.5, 1.5)
                            child.userData.onClick = () => {
                                this.css3dObject.visible = true;
                            }
                        }
                        if (child.name.includes('立方体框活动')) {
                            child.visible = true;
                            for (let i = 0; i < 10; i++) {
                                let goods = child.clone()
                                goods.name = "goods" + i;
                                // goods.position.set(-95,0.4,9)
                                goods.position.set(-95, 0.8, 44.4)
                                this.goodsArray.push(goods)
                                this.scene.add(goods);
                            }
                        }
                        if (child.name == '仓库运货架机械') {
                            child.position.set(child.position.x, -1.7, child.position.z)
                            this.goods1 = child

                        }
                        if (child.name == '仓库运货架机械001') {
                            child.position.set(child.position.x, -1.7, child.position.z)
                            this.goods001 = child
                        }
                        if (child.name == '仓库运货架机械合集') {
                            // 仓库运货机械
                            this.goodsAll = child
                            child.visible = true;
                        }
                        if (child.name.includes('仓库货架')) {
                            // 仓库运货机械
                            // child.userData.onClick = async () => {
                            //     getEl(0)
                            // }
                        }
                    });

                    let arrnew = this.machinery2.sort((a, b) => {
                        return a.sortIndex - b.sortIndex
                    });
                    let arrnew2 = this.machinery1.sort((a, b) => {
                        return a.sortIndex - b.sortIndex
                    });
                    // console.log(arrnew,666)
                    console.log(this.machinery1)
                    this.scene.add(mesh);
                    this.render3d()

                    this.controls.addEventListener('change', (e) => {
                        this.css3dObject.visible?this.css3dObject.lookAt(this.camera.position.x, 0, this.camera.position.z):"";
                        // console.log(79,this.camera.position)
                     })
                    resolve(this)
                });
            });
        })
    }


}