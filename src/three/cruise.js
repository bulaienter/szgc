import * as THREE from "three";
class Cruise {
    constructor(controlsSelf,boxList,followLight) {
        this.forward=null;
        this.back=null;
        this.left=null;
        this.right=null;
        this.canJump=false;
        this.accelerated=false;
        this.velocity=new THREE.Vector3()
        this.raycaster=new THREE.Raycaster(
            new THREE.Vector3(),
            new THREE.Vector3(0, -1, 0), 0, 2
        );
        this.followLight=followLight;
        controlsSelf.lock()
        this.controlsSelf=controlsSelf;

        this.boxList=boxList
    }

    handleKey(){
        let keyUp = (e) => {
            switch(e.code){
                case "KeyW": //前
                case "ArrowUp":
                    this.forward = false
                    break
                case "KeyA": //左
                case "ArrowLeft":
                    this.left = false
                    break
                case "KeyD": //右
                case "ArrowRight":
                    this.right = false
                    break
                case "KeyS": //后
                case "ArrowDown":
                    this.back = false
                    break
                case "ShiftLeft": // 加速
                    this.accelerated = false
                    break
            }
        }
        let keyDown = (e) => {
            console.log(e.code)
            switch(e.code){
                case "KeyW": //前
                case "ArrowUp":
                    this.forward = true
                    break
                case "KeyA": //左
                case "ArrowLeft":
                    this.left = true
                    break
                case "KeyD": //右
                case "ArrowRight":
                    this.right = true
                    break
                case "KeyS": //后
                case "ArrowDown":
                    this.back = true
                    break
                case "ShiftLeft": //加速
                    this.accelerated = true
                    break
                case "Space": // 跳
                    if(this.canJump){
                        this.velocity.y += 30
                    }
                    this.canJump = false
                    break
            }
        }
        document.addEventListener("keyup", keyUp, false)
        document.addEventListener("keydown", keyDown, false)

    }

    //根据传入角度判断附近是否有障碍物
    //移动是根据按键决定的 在按下左键的时候进行左侧检测 右侧就右侧检测 利用射线判断有没有与物体相交 如果撞到物体上了就阻止这一侧的移动
    collideCheck(angle){
        let rotationMatrix = new THREE.Matrix4()
        rotationMatrix.makeRotationY(angle * Math.PI / 180)
        const cameraDirection = this.controlsSelf.getDirection(new THREE.Vector3(0, 0, 0)).clone()
        cameraDirection.applyMatrix4(rotationMatrix)
        const raycaster = new THREE.Raycaster(this.controlsSelf.getObject().position.clone(), cameraDirection, 0, 2)
        raycaster.ray.origin.y -= 4
        const intersections = raycaster.intersectObjects(this.boxList, true)
        return intersections.length
    }

    move(delta){
        let speed=0.3
        if(this.controlsSelf.isLocked){
            this.velocity.x = 0
            this.velocity.z = 0
            this.velocity.y -= 70 * delta // 下降速度
            //获取相机位置
            let position = this.controlsSelf.getObject().position
            //设置射线原点
            this.raycaster.ray.origin.copy(position)
            this.raycaster.ray.origin.y -= 4
            //检测所有相交的物体 
            let intersects = this.raycaster.intersectObjects(this.boxList, false)
            //自带的方法判断需要过滤
            if(intersects.length){
                this.velocity.y = Math.max(0, this.velocity.y)
                this.canJump = true
            }

            if(this.forward || this.back){
                //向前才可加速
                this.velocity.z = (Number(this.forward) - Number(this.back)) * speed + (this.forward ? Number(this.accelerated)*0.5 : 0)
            }
            if(this.left || this.right){
                this.velocity.x = (Number(this.right) - Number(this.left)) * speed + Number(this.accelerated)*0.5
            }

            //四个方位是否产生碰撞
            let leftCollide = false
            let rightCollide = false
            let forwardCollide = false
            let backCollide = false
            //碰撞检测 collide check
            if (this.forward) forwardCollide = this.collideCheck(0)
            if (this.back) backCollide = this.collideCheck(180)
            if (this.left) leftCollide = this.collideCheck(90)
            if (this.right) rightCollide = this.collideCheck(270)

            //右侧有障碍物时向右移动 置零
            if ((this.right && rightCollide) || (this.left && leftCollide)) {
                this.velocity.x = 0
            }
            //前方有障碍物时向前移动 置零
            if ((this.forward && forwardCollide) || (this.back && backCollide)) {
                this.velocity.z = 0
            }
            //设置控制器移动
            this.controlsSelf.moveRight(this.velocity.x)
            this.controlsSelf.moveForward(this.velocity.z)
            this.controlsSelf.getObject().position.y += this.velocity.y * delta
            //还原起始高度
            if(this.controlsSelf.getObject().position.y < 5){
                this.velocity.y = 0
                this.controlsSelf.getObject().position.y = 5
                this.canJump = true
            }

            //设置光源大小及跟随相机移动
            this.followLight.intensity = 2
            this.followLight.position.set(...position)
        }
    }

}

export default Cruise