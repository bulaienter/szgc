import {createRouter, createWebHashHistory} from 'vue-router';

// 路由列表
const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('../views/index.vue')
    },
    {
        path: '/npc',
        name: 'npc',
        component: () => import('../views/components/Npc.vue')
    },
    {
        path: '/model',
        name: 'model',
        component: () => import('../views/components/Model.vue')
    },
    {
        path: '/perspective',
        name: 'perspective',
        component: () => import('../views/perspective/perspective.vue')
    },
]

// 导出路由
export default  createRouter({
    history: createWebHashHistory(),
    routes
})