function setRem() {
    // 以1920分辨率为基准，将视口分为120份，每一份为16px，即1rem=16px
    const screenWidth = 1920
    const scale = screenWidth / 16
    const htmlWidth = document.documentElement.clientWidth || document.body.clientWidth
    const htmlDom = document.getElementsByTagName('html')[0]
    // 设置根元素字体大小
    htmlDom.style.fontSize = htmlWidth / scale + 'px'
}
// 初始化
setRem()
// 改变窗口大小时重新设置 rem
window.onresize = function() {
    setRem()
}