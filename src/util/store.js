import { defineStore } from 'pinia'

export const  useStatusDataStore = defineStore('statusData',{ 
    state: () => ({
      showStatus: 1,
      workId:0,
      deviceDetails:{}
    }),
    
  })
  