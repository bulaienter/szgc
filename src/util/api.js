import myAxios from './request'

// 生产线任务统计
export function getProductionLineTask(data) {
    return myAxios({
        url: 'mes/result/getProductionLineTask',
        method: 'get',
        data
    })
}


// 生产工单任务统计
export function getWorkorders(data) {
    return myAxios({
        url: 'mes/result/getWorkorders',
        method: 'get',
        data
    })
}

// 非实时工艺   查看每个任务的工艺工序以及每个工序对应的时间
export function getProcess(data) {
    return myAxios({
        url: `mes/result/getProcess/${data.id}`,
        method: 'get',
        data
    })
}
// 当月产量、当日产量
export function getDayAndMonthProduction(data) {
    return myAxios({
        url: `mes/result/getDayAndMonthProduction`,
        method: 'get',
        data
    })
}

// 设备列表
export function getDeviceList(data) {
    return myAxios({
        url: `iot/result/getDeviceList`,
        method: 'get',
        params:data
    })
}

// 设备预警&设备告警
export function getDeviceAlertList(data) {
    return myAxios({
        url: `iot/result/getDeviceAlertList`,
        method: 'get',
        data
    })
}


// 温度数据
export function selectTemperatureList(data) {
    return myAxios({
        url: `iot/result/selectTemperatureList`,
        method: 'get',
        data
    })
}

// 货物存放情况
export function selectStorageOfCargo(data) {
    return myAxios({
        url: `iot/result/selectStorageOfCargo`,
        method: 'get',
        data
    })
}

// 获取天气信息
export function getWeather(data) {
    return myAxios({
        url: `mes/result/weather/101110101`,
        method: 'get',
        data
    })
}



// 生产能耗统计（模拟数据）
export function produceEnergyConsumption(data) {
    return myAxios({
        url: `mes/result/produceEnergyConsumption`,
        method: 'get',
        data
    })
}

// 工艺流程步骤
export function processFlowSteps(data) {
    return myAxios({
        url: `mes/result/processFlowSteps/${data.id?data.id:0}`,
        method: 'get',
        data
    })
}


//请求挂件的json文件
export function getJsonModel(data) {
    return myAxios({
        url: `getjson/xf/jointMes/api/threeDimensions/selectModelById`,
        method: 'post',
        data
    })
}


