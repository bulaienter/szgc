import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import './util/rem.js'
import { createPinia } from 'pinia';

let pinia=createPinia()
import router from './router/index.js'
createApp(App).use(pinia).use(router).mount('#app')
